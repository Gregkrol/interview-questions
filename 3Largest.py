#!/usr/bin/python
#
# Find the 3 largest numbers in a list
# http://www.programminginterviews.info/2011/05/simple-coding-questionspart-1.html
#
# Write clean code

def main():
	cont = True
	numbers = []
	
	print "Please fill up a list with random numbers. Terminate with the character 'n'"
	
	while(cont):
		numbers.append(raw_input('Next number >'))
		if(numbers[-1] == 'n'):
			numbers.remove(numbers[-1])
			cont = False
		
	#Find the first three largest
	sortedNums = sorted(numbers, reverse=True) #http://wiki.python.org/moin/HowTo/Sorting#Ascending_and_Descending
	
	for x in range(3):
		print sortedNums[x]
		

if __name__ == "__main__":
	main()