# Determine if a number is a perfect square or not
# from: http://www.programminginterviews.info/2011/05/how-to-find-if-number-is-perfect-square.html
# Greg Krol
# Bitbucket.org/GregKrol

def isPerfectSquare(num):
    # returns True if num is a perfect square
    # else return False

    num = int(num)

    if (num <= 1):
        return True

    currSquare = 4
    currNum = 2

    while (currSquare <= num):

        if (currSquare == num):
            return True

        currNum += 1
        currSquare = currNum*currNum

    return False


def main():
    text = "A simple program to test if a number is a \n"
    text += "\tPerfect Square or not\n"

    print(text)

    again = True
    while(again):
        number = input("Number >")

        if(isPerfectSquare(number)):
            print(str(number) + " is a perfect square")
        else:
            print(str(number) + " is NOT a perfect square")

        text = "try again? (y/n)"
        if(input(text + " >") != 'y'):
            again = False

if __name__ == "__main__":
    main()