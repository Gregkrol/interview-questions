/**
 * 
 */
package iq;

//import java.util.Vector;

/**
 * @author Cynikre
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public final static void main(String[] args) {
		// Simple String manipulation
		
		String simpleString = "This is a String";
		String secondSimpleString = "So is this";
		
		//Vector<Character> letters = new Vector<Character>();
		
		// Print the length of the string
		System.out.println(getLength(simpleString));
		
		// Print the result of copying a string char for char
		System.out.println(copyString(simpleString));
		
		// concatinate two strings
		System.out.println( concatStrings(simpleString, secondSimpleString) );
		
		// Print the letters of a string
		printLetters(simpleString);
		
		// Print if the letter is a palindrome or not
		if (isPalindrome(simpleString)){
			System.out.println(simpleString + "is a Palindrome");
		}
		else {
			System.out.println(simpleString + " is NOT a Palindrome");
		}
	}
	
	private static String getLength(String stringIn){
		String result = "String length: " + stringIn.length();
		return result;
	}
	
	private static String copyString(String stringIn){
		// Copy a string to another char by char
		char simpleCharArray[] = new char[stringIn.length()];
		
		for ( int i = 0; i < stringIn.length(); i++) {
			simpleCharArray[i] = stringIn.charAt(i);
		}
		
		String secondSimpleString = new String(simpleCharArray);
		
		// NOTE: simpleCharArray.toString gives us garbage
		return "Copied String: " + secondSimpleString;		
	}

	private static String concatStrings(String stringBegin, String stringEnd) {
		// return string that is the concatination of the two strings via chars
		
		int resultLength = stringBegin.length() + stringEnd.length();
		char charArr[] = new char[resultLength];
		
		for (int i = 0; i < resultLength; i++){
			if (i < (stringBegin.length())){
				charArr[i] = stringBegin.charAt(i);
			}
			else {
				charArr[i] = stringEnd.charAt(i - stringBegin.length());
			}
		}
		
		String result = new String(charArr);
		return "Concatination of \"" + stringBegin 
			+ "\" and \"" + stringEnd + "\" :" 
			+ result;
	}
	
	private static void printLetters(String StringIn){
		for ( int i = 0; i < StringIn.length(); i++ ){
			System.out.println(StringIn.charAt(i));
		}
	}
	
	private static boolean isPalindrome(String StringIn){
		// Return true if StringIn is a palindrome; else False
		//TODO
		
		
		return false;
	}
}
