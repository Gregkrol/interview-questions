#/usr/bin/python
# 
# LinkedList.py
# 
# Because python doesn't explicitly do pointers, we will impliment linked lists
# with a class & object oriented programming
# http://www.programminginterviews.info/2011/05/linked-lists-demystified.html
#

class node:
	def __init__(self, previous = None, data = None, next = None):
		self.p = previous
		self.n = next
		self.d = data
		
	def SetData(data):
		self.d = data
		
	def SetNext(next):
		self.n = next
	
	def SetPrevious(previous):
		self.p = previous
		
	def GetData():
		return self.d
	
	
class LinkedList:
	def __init__(self, head = None, tail = None,):
		self.nodes = []
		self.h = head
		self.t = tail
		
	def append(node):
		self.t.SetNext(node.prev