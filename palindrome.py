#!/usr/bin/python
# palindrome.py
#
# Find if a number is a palindrome or not

def isPalindrome(word):
	# take in a string, and check if it's a palindrome
	firstHalf = word[0:len(word)/2]
	secondHalf = word[len(word)/2:len(word)]
		
	secondHalf = secondHalf[::-1] # stride notation 
	print firstHalf,secondHalf

	# compare the two halves
	if (cmp(firstHalf,secondHalf) == 0):
		return True
	
	return False
	
def betterIsPalindrome(word):
	# a better way of checking if a word/number is a palindrome
	# The problem with isPalindrome is it doesn't take into 
	# consideration odd numbered strings
	
	for i in range(len(word)):
		if not (word[i] == word[-1-i]):
			return False
		
	return True
	
def main():
	text = "Palindrome.py\n"
	text += "\nInput any word, or group of numbers and I'll let you know\n"
	text += "\tif it's a palindrome or not.\n"
	
	print text
	
	word = raw_input("Is it a palindrome > ")
	word = str(word)
	
	# isPalindrome
	print "isPalindrome"
	if(isPalindrome(word)):
		print word + " sure is a palindrome"
	else:
		print word + " is not a palindrome"
	
	# betterIsPalindrome
	print "\nbetterIsPalindrome"
	if(betterIsPalindrome(word)):
		print word + " sure is a palindrome"
	else:
		print word + " is not a palindrome"
		
if __name__ == "__main__":
	main()