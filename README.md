﻿# Interview Questions Readme

[http://www.programminginterviews.info](http://www.programminginterviews.info)

Heres some programs, written in python, to answer some interview questions.
Some files only have the question, and no answer. 

Lets do this. 

In the Java folder, you'll find project files for Eclipse that has answers in Java
- - -

## Basic Interview Questions

### 3Largest.py
  
find the 3 largest numbers in a list. 

### Largest.py

find the x largest numbers in a list
    
### palindrome.py

determine if a number or word is a palindrome or not
    
### Matching.py

find the index of the first character of the occurance of sring X in string Y
    
### PerfectSquare.py

Determine if a number is a perfect square or not
    
### PerfectSquareBenchmarks.py

3 algorithms to find if a number is a perfect square or not, and the times they take
    
### StringManip.py

basic string stuff
    
### Stringsgalore.py

String manipulation and comparison
    
### LinkedList.py

implementation of Linked Lists class in Python
