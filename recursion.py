#!/usr/bin/python
#
# Recursion
# http://www.programminginterviews.info/2011/05/recursionconcepts-and-code.html
# 
# By Greg Krol bitbucket.org/Gregkrol

def factorial(number):
	number = int(number)
	if (number <= 1):
		return 1
	else:
		return number * factorial(number -1)
		
def fibinacci(number):
	if (number <= 1):
		return 1
	else:
		return (fibinacci(number - 1) + fibinacci(number - 2))

def gcd(number1, number2):
	remainder = number1 % number2
	
	if (remainder == 0):
		return number2
	else:
		return gcd(number2, remainder)
	
		
def main():
	text = "Various recursive functions"
	
	num = raw_input("Number > ")
	
	print "Factorial : " + str(factorial(int(num)))
	print "Fibinacci : " + str(fibinacci(int(num)))
	
	num2 = raw_input("\nSecond number for GCD > ")
	
	print "\nGCD : " + str(gcd(int(num),int(num2)))
	
	
if __name__ == "__main__":
	main()