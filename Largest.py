#!/usr/bin/python
#
# Find the 3 largest numbers in a list
# http://www.programminginterviews.info/2011/05/simple-coding-questionspart-1.html
# 
# I felt that my previous example wasn't good enough. There exists an idea that you
# should only write code that solves the problem. No more. 
# 
# Write clean code

def largest(listIn,x):
	# return the largest item of a given list
	result = []
	x = int(x)
	sortedList = sorted(listIn, reverse=True)
	
	for i in range(x):
		result.append(sortedList[i])
		
	return result
	

def main():
	cont = True
	numbers = []
	
	text = "Please fill up a list with any numbers you want.\n Terminate with the character 'n' \n Then enter the number of numbers you want back\n"
	print text
	
	while(cont):
		numbers.append(raw_input('Next number > '))
		if(numbers[-1] == 'n'):
			numbers.remove(numbers[-1])
			cont = False
			
	a = raw_input('Number of largest numbers > ')
	
	print "\nThe largest "+a+" numbers are:" 
	largestNumbers = largest(numbers, a)
	
	nums = ""
	for item in largestNumbers:
		nums = nums + item + ","
		
	print nums

if __name__ == "__main__":
	main()