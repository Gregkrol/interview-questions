#!/usr/bin/python
# 
# String Manipulation can be fun
# http://www.programminginterviews.info/2011/05/string-manipulation-can-be-fun.html
#
# 

def strLen(string):
	i = 0
	for letter in string:
		i += 1
	
	return i
	
def strCopy(string1, string2):
	string2 = string1
	return string2
	
def strConcat(string1, string2):
	return string1 + string2
	